output "service_role_name" {
  value = "${aws_iam_role.service.name}"
  description = "Instance IAM service role name"
}

output "ec2_role_id" {
  value = "${aws_iam_role.ec2.id}"
  description = "Instance IAM ec2 role id"
}

output "instance_profile_name" {
  value = "${aws_iam_instance_profile.ec2.name}"
  description = "Instance IAM instance profile name"
}
